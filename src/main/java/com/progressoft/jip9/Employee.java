package com.progressoft.jip9;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Employee {


    private Employee() {
    }

    public String getFirstName() {
        return null;
    }

    public String getLastName() {
        return null;
    }

    public String getBirthPlace() {
        return null;
    }

    public LocalDate getBirthDate() {
        return null;
    }

    public LocalDate getHiringDate() {
        return null;
    }

    public LocalDate getResignationDate() {
        return null;
    }

    public String getPosition() {
        return null;
    }

    public BigDecimal getSalary() {
        return null;
    }

    public static class Builder {


        public Builder setFirstName(String firstName) {
            return this;
        }

        public Builder setLastName(String lastName) {
            return this;
        }

        public Builder setBirthPlace(String birthPlace) {
            return this;
        }

        public Builder setBirthDate(LocalDate birthDate) {
            return this;
        }

        public Builder setHiringDate(LocalDate hiringDate) {
            return this;
        }

        public Builder setResignationDate(LocalDate resignationDate) {
            return this;
        }

        public Builder setPosition(String position) {
            return this;
        }

        public Builder setSalary(BigDecimal salary) {
            return this;
        }

        public Employee build() {
            return new Employee();
        }
    }
}
